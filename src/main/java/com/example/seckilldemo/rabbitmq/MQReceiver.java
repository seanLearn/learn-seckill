package com.example.seckilldemo.rabbitmq;

import com.example.seckilldemo.pojo.SeckillOrder;
import com.example.seckilldemo.pojo.User;
import com.example.seckilldemo.service.IGoodsService;
import com.example.seckilldemo.service.IOrderService;
import com.example.seckilldemo.utils.JsonUtil;
import com.example.seckilldemo.vo.GoodsVo;
import com.example.seckilldemo.vo.SeckillMessageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author 59244
 * @date 2021/11/27 10:54
 */
@Service
@Slf4j
public class MQReceiver {

    @Autowired
    IGoodsService goodsService;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    IOrderService orderService;

    @RabbitListener(queues = "seckill_queue")
    public void receiveSeckillMessage(String msg){
        log.info("接收消息："+msg);
        SeckillMessageVo seckillMessageVo = JsonUtil.jsonStr2Object( msg, SeckillMessageVo.class);
        User user = seckillMessageVo.getUser();
        Long goodsId = seckillMessageVo.getGoodsId();
        GoodsVo goodsVo = goodsService.findGoodsVoByGoodsId(goodsId);
        //判断库存
        if(goodsVo.getStockCount() <= 0){
            return ;
        }
        //判断重复购买
        SeckillOrder seckillOrder
                = (SeckillOrder) redisTemplate.opsForValue().get("seckillOrder:" + user.getId() + "+" + goodsId);
        if(seckillOrder != null){
            return ;
        }
        //下单
        orderService.seckillToOrder(user,goodsVo);
    }




    @RabbitListener(queues = "queue")
    public void receive(Object msg){
        log.info("接收消息："+msg);
    }

    /**
     * 功能描述：接受fanout交换机管理的queue的消息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:39
     */
    @RabbitListener(queues = "queue_fanout01")
    public void receiveFanout01(Object msg){
        log.info("queue_fanout01接收消息："+msg);
    }
    @RabbitListener(queues = "queue_fanout02")
    public void receiveFanout02(Object msg){
        log.info("queue_fanout02接收消息："+msg);
    }

    /**
     * 功能描述：接受direct交换机管理的queue的消息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:40
     */
    @RabbitListener(queues = "queue_direct01")
    public void receiveDirect01(Object msg){
        log.info("queue_direct01接收消息："+msg);
    }
    @RabbitListener(queues = "queue_direct02")
    public void receiveDirect02(Object msg){
        log.info("queue_direct02接收消息："+msg);
    }

    /**
     * 功能描述：接受topic交换机管理的queue的消息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:48
     */
    @RabbitListener(queues = "queue_topic01")
    public void receiveTopic01(Object msg){
        log.info("queue_topic01接收消息："+msg);
    }
    @RabbitListener(queues = "queue_topic02")
    public void receiveTopic02(Object msg){
        log.info("queue_topic02接收消息："+msg);
    }
}
