package com.example.seckilldemo.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TODO rabbitmq的生产者
 *
 * @author 59244
 * @date 2021/11/27 10:47
 */
@Service
@Slf4j
public class MQSeander {

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void sendSeckillMessage(String msg){
        log.info("发送消息："+msg);
        rabbitTemplate.convertAndSend("topicExchange","seckill.message",msg);
    }

    public void send(String msg){
        log.info("发送消息："+msg);
        rabbitTemplate.convertAndSend("queue",msg);
    }
    /**
     * 功能描述：给fanout模式的交换机发送消息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:39
     */
    public void sendFanout(String msg){
        log.info("发送消息Fanout："+msg);
        rabbitTemplate.convertAndSend("fanoutExchange","",msg);
    }

    /**
     * 功能描述：给direct模式的交换机发送消息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:39
     */
    public void sandDirectRed(String msg){
        log.info("发送消息DirectRed："+msg);
        rabbitTemplate.convertAndSend("directExchange","queue.red",msg);
    }
    public void sandDirectGreen(String msg){
        log.info("发送消息DirectGreen："+msg);
        rabbitTemplate.convertAndSend("directExchange","queue.green",msg);
    }

    /**
     * 功能描述：给topic模式的交换机发送消息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:46
     */
    public void sendTopicOne(String msg){
        log.info("发送消息TopicOne："+msg);
        rabbitTemplate.convertAndSend("topicExchange","queue.a.b",msg);
    }
    public void sendTopicTwo(String msg){
        log.info("发送消息TopicTwo："+msg);
        rabbitTemplate.convertAndSend("topicExchange","c.queue.a.b",msg);
    }
}
