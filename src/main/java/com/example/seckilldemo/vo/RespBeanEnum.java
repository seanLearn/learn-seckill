package com.example.seckilldemo.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum RespBeanEnum {
    //通用
    SUCCESS(200, "成功"),
    ERROR(500, "服务器异常"),
    //登录
    LOGIN_ERROR(500200,"用户名或密码不正确"),
    PASS_ERROR(500200,"密码不正确"),
    MOBILE_ERROR(500201,"手机号格式不正确"),
    BIND_ERROR(500202,"参数校验异常"),
    MOBILE_NOT_EXIST(500203,"手机号不存在"),
    PASSWORD_UPDATE_FAIL(500204,"密码更新失败"),
    SEAAION_ERROR(500205,"用户不存在"),
    //秒杀
    EMPTY_ERROR(500300,"库存不足，不可以秒杀"),
    REPEAT_ERROR(500301,"不可以重复秒杀"),
    ORDER_NOT_EXIST(500302,"订单不存在"),
    REQUEST_ILLEGAL(500303,"请求非法"),
    CAPTCHA_ERROR(500304,"验证码错误"),
    REQUEST_LOTS(500305,"请求过于频繁，请稍后再试"),
    ;


    private final Integer code;
    private final String message;
}
