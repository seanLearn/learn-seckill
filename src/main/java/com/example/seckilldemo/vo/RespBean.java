package com.example.seckilldemo.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespBean {

    private int code;
    private String message;
    private Object obj;

    /**
     * 功能描述：成功后返回结果
     *
     * @author: sean
     * @time: 2021/11/14 16:37
     */
    public static RespBean success(){
        return new RespBean(RespBeanEnum.SUCCESS.getCode(), RespBeanEnum.SUCCESS.getMessage(),null);
    }

    /**
     * 功能描述：成功后返回结果
     *
     * @author: 59244
     * @time: 2021/11/14 16:37
     */
    public static RespBean success(Object obj){
        return new RespBean(RespBeanEnum.SUCCESS.getCode(), RespBeanEnum.SUCCESS.getMessage(), obj);
    }

    /**
     * 功能描述：失败后返回结果
     *
     * @author: 59244
     * @time: 2021/11/14 16:38
     */
    public static RespBean error(RespBeanEnum respBeanEnum){
        return new RespBean(respBeanEnum.ERROR.getCode(), respBeanEnum.getMessage(), null);
    }

    /**
     * 功能描述：失败后返回结果
     *
     * @author: 59244
     * @time: 2021/11/14 16:38
     */
    public static RespBean error(RespBeanEnum respBeanEnum, Object obj){
        return new RespBean(respBeanEnum.ERROR.getCode(), respBeanEnum.getMessage(), obj);
    }

}
