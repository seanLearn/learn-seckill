package com.example.seckilldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.seckilldemo.pojo.SeckillGoods;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {

}
