package com.example.seckilldemo.controller;

import com.example.seckilldemo.service.IUserService;
import com.example.seckilldemo.vo.LoginVo;
import com.example.seckilldemo.vo.RespBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("login")
@Slf4j
public class LoginController {

    @Autowired
    private IUserService userService;

    /**
     * 功能描述：跳转到登陆页面
     *
     * @params: 
     * @return： 
     * @author: 59244
     * @time: 2021/11/14 17:10
     */
    @RequestMapping("tologin")
    public String toLogin(){
        return "login";
    }

    /**
     * 功能描述：登录功能
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/14 17:10
     */
    @RequestMapping("dologin")
    @ResponseBody
    public RespBean dologin(@Valid LoginVo loginVo, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){

        return userService.dologin(loginVo, httpServletRequest, httpServletResponse);
    }
}
