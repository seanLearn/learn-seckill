package com.example.seckilldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.seckilldemo.pojo.Goods;
import com.example.seckilldemo.vo.GoodsVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
public interface IGoodsService extends IService<Goods> {

    /**
     * 功能描述：获取商品列表
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/20 15:22
     */
    List<GoodsVo> findGoodsVo();

    /**
     * 功能描述：通过商品id获取商品信息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/20 16:27
     * @param goodsID
     */
    GoodsVo findGoodsVoByGoodsId(Long goodsID);
}
