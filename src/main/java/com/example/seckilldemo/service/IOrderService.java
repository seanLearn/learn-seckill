package com.example.seckilldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.seckilldemo.pojo.Order;
import com.example.seckilldemo.pojo.User;
import com.example.seckilldemo.vo.GoodsVo;
import com.example.seckilldemo.vo.OrderDetailVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
public interface IOrderService extends IService<Order> {

    /**
     * 功能描述：秒杀 生成订单
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/20 18:34
     */
    Order seckillToOrder(User user, GoodsVo goodsVo);

    /**
     * 功能描述：根据订单id获取订单详情信息（订单信息和商品信息）
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/22 11:41
     */
    OrderDetailVo detailById(Long orderId);

    /**
     * 功能描述：生成用户独有的秒杀地址
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/28 14:13
     */
    String createPath(User user, Long goodsId);

    /**
     * 功能描述：检查该请求路径 是否是该用户的路径
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/28 14:22
     */
    Boolean checkPath(String path, User user, Long goodsId);

    /**
     * 功能描述：检验验证码
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/28 15:45
     */
    Boolean checkCaptcha(String captcha, User user, Long goodsId);
}
