package com.example.seckilldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.seckilldemo.pojo.SeckillOrder;
import com.example.seckilldemo.pojo.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
public interface ISeckillOrderService extends IService<SeckillOrder> {

    Long getresult(User user, Long goodsId);
}
